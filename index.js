/*
	npm init -y
	npm i express -y
	npm i mongoose -y
	touch .gitignore (node_modules)
*/

const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3001;

//[SECTION] MongoDB connection
//Syntax -> mongoose.connect("SRV LINK", {useNewUrlParser: true, useUnifiedTopology: true})

mongoose.connect("mongodb+srv://admin:admin123@batch218-to-do.tb0qo8f.mongodb.net/toDo?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "conection error"));

db.once("open", () => console.log("We're connected to the cloud database"));

//[SECTION] Mongoose Schemas
//It determines the structure of our documents to be stored in the database
//It acts as our data blueprint and guide


const taskSchema = new mongoose.Schema({
	name : {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});


//[SECTION] Models
const Task = mongoose.model("Task", taskSchema);



//[SECTION] Creation of to do list routes

app.use(express.json());
//allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

// Creating a new task


app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err, result) =>{
		if(result != null && result.name == req.body.name){
			return res.send("Duplicate found");
		}else{
			let newTask= new Task({
				name: req.body.name
			});

			newTask.save((saveErr, saveTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})
})

//Get method
app.get("/tasks", (req, res) => {
	Task.find({}, (err, result) => {
		if(err){
			return console.log(err);
		}else{
			return res.status(200).json({
				data : result
			})
		}
	})
})

// End of Discussion

// ACTIVITY
/*
	1. Create a User schema.
		username - string
		password - string
	2. Create a User model.
	Take a screenshot of your mongoDB collection to show that the users collection is added.
*/
	// #1
	const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required: [true, "Username is required"]
	},
	password : {
		type: String,
		required: [true, "Password is required"]
	}
});

	// #2
	const User = mongoose.model("User", userSchema);

/*
	3. Create a route for creating a user, the endpoint should be “/signup” and the http method to be used is ‘post’.
	4. Use findOne and conditional statement to check if there is already an existing username. If there is already, the program should send a response “Duplicate user found.” If there is no match  (else), it should successfully create a new user with password.
	5. If there is no error encountered during the process (include status code 201), the program should send a response “New user registered”

*/
	// #3
	app.post("/signup", (req, res) => {
		User.findOne({username : req.body.username}, (err, result) => {
			if(result != null && result.username == req.body.username){
				return res.send("Duplicate user found.")
			}
			else{
				let newUser = new User({
					username : req.body.username,
					password : req.body.password
				})

				newUser.save((saveErr, saveUser) => {
					if(saveErr){
						return console.log(saveErr);
					}
					else{
						return res.status(201).send("New user registered.")
					}
				})
			}
		})
	})


	// #4 & #5
	app.get("/users", (req, res) => {
		User.find({}, (err, result) => {
			if(err){
				return console.log(err);
			}
			else{
				return res.status(200).json({
					data : result
				})
			}
		})
	})

app.listen(port, () => console.log(`Server running at port ${port}`));
